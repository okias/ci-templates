## This file lists the possible keys supported for a distribution-specific
## YAML file.

## The root level entry must be unique, but not necesseraly tied to a
## distribution. Each root level entry will generate a new template in
## the `templates` folder.

example:
  ## @distribution: The distribution name
  ##
  ## Each template will be given the `{distribution}.yml` name
  ##
  ## Mandatory
  distribution: ""

  ## @image: The docker image name
  ##
  ## Only required where the image name differs from the standard
  ## $distribution:$version format.
  image:

  ## @fixed_version: There is only one version for this distribution
  ##
  ## If set to true, the versions: field must contain the name of the single
  ## version.
  fixed_version: false

  ## @versions: Which versions are tested in the CI
  ##
  ## Must include all versions tested in the CI. For fixed-version
  ## distributions, must include the single version name they use (rolling,
  ## latest, etc.)
  versions: []

  ## @version_map: A dictionary to use for package source lookups
  ##
  ## This is needed where a version cannot be used for package repositories.
  ## e.g. 19.10 does not resolve, Ubuntu needs to use 'Eoan' instead
  ##
  ## Currently only supported for/used by ubuntu
  version_map:
      'v1': 'funny name'
      'v2': 'even funnier name'
      'v3': 'oh how we laughed'

  ## @package_type: The package type supported by the distribution
  ##
  ## This has no "real" effect but to use debs on debian/ubuntu and rpms on
  ## fedora/centos
  ##
  ## Mandatory
  package_type: ""

  ## @prepare: The list of commands that are used to prepare the container image for package installations
  ##
  ## This needs to be a list
  prepare: []

  ## @upgrade: The list of commands that are used to upgrade the container image
  ##
  ## This needs to be a list
  ##
  ## Mandatory
  upgrade: []

  ## @install: The list of commands that are used to install a package in
  ##           the container image
  ##
  ## This needs to be a list
  ##
  ## Mandatory
  install: []

  ## @clean: The list of commands that are used to remove leftovers from
  ##         the package manager in the final container image
  ##
  ## This needs to be a list
  ##
  ## Mandatory
  clean: []

  ## @aarch64: is the upstream docker image aarch64 capable
  ##
  ## boolean
  aarch64: false

  ## @qemu: qemu-specific configuration
  ##
  qemu:

    ## @qemu.packages: list of packages that are required on a base qemu image
    ##
    ## if this variable is not defined, the matching qemu template will not
    ## be generated
    packages: []
